class BUandBook:
    def __init__(self, input):
        self.alphabet = self.get_alphabet()
        self.input = input.split(" ")

    @staticmethod
    def get_alphabet():
        lst_symbols = []
        try:
            with open('text_2.txt', encoding='utf8') as f:
                lines = f.readlines()
                if not lines:
                    return
                line_1 = lines[0].split()
                for symbol in line_1:
                    lst_symbols.append(symbol)
        except:
            return
        return lst_symbols

    def get_ciclic_table(self):
        ciclic_table = []
        for i in range(len(self.input)):
            ciclic_table.append("".join(self.input[i:] + self.input[:i]))
        return ciclic_table

    def get_BU_code_word_and_index(self):
        ciclic_table = self.get_ciclic_table()
        sorted_table = sorted(ciclic_table)
        index = sorted_table.index(ciclic_table[0]) + 1
        output = "".join(i[-1] for i in sorted_table)
        return output, index

    def transform_alphabet(self, index):
        new_alphabet = [None] + self.alphabet
        new_alphabet[0] = new_alphabet.pop(index + 1)
        new_alphabet = [i for i in new_alphabet if i is not None]
        self.alphabet = new_alphabet

    def get_MTF_code(self, encoded_wold):
        code = []
        for i in encoded_wold:
            index= self.alphabet.index(i)
            code.append(index)
            self.transform_alphabet(index)
        return code

    def encode(self):
        encoded_wold, index = self.get_BU_code_word_and_index()
        code = self.get_MTF_code(encoded_wold)
        return code, encoded_wold, index


if __name__ == '__main__':
    bu = BUandBook(input("введите: "))
    code, encoded_wold, index = bu.encode()
    code = " ".join(str(i) for i in code)
    print(f"код: {code}\nвыходное слово БУ: {encoded_wold}\nиндекс: {index}")
