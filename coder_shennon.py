import math


def read_file():
    """
        Читает файл с алфавитом и вероятностями и возвращает
        словарь из них, отсортированный по не возрастанию вероятностей
    """
    dictionary = dict()
    try:
        with open('text.txt', encoding='utf8') as f:
            lines = f.readlines()
            if not lines:
                return
            line_1 = lines[0].split()
            line_2 = lines[1].split()
            if len(line_1) != len(line_2):
                return
            for num_1, symbol in enumerate(line_1):
                for num_2, probability in enumerate(line_2):
                    if num_1 == num_2:
                        dictionary[symbol] = probability
        dict_sort = dict(sorted(dictionary.items(), key=lambda item: item[1], reverse=True))
    except:
        return
    return dict_sort


def get_symbols(dict_sort):
    """
        Возвращает список из символов алфавита,
        проходясь по полученному при вызове функции read_file() словарю
    """
    lst_symbols = []
    for i in dict_sort:
        lst_symbols.append(i)
    return lst_symbols


def get_lst_with_length(dict_sort):
    """
        Возвращает список из длин конечных кодовых слов каждого символа,
        проходясь по полученному при вызове функции read_file() словарю
        и вычисляя с помощью библиотеки math значения этих длин ( l = log2(1/p) )
    """
    lst_length_i = []
    for i in dict_sort:
        length_i = math.ceil(math.log2(1 / float(dict_sort[i])))
        lst_length_i.append(length_i)
    return lst_length_i


def get_sum_probabilities(dict_sort):
    """
        Возвращает список из сумм вероятностей предыдущих символов,
        проходясь по полученному при вызове функции read_file() словарю
    """
    sum_probabilities = 0.0
    lst_sum_probabilities = [0.0]
    for i in dict_sort:
        sum_probabilities += float(dict_sort[i])
        lst_sum_probabilities.append(sum_probabilities)
    del lst_sum_probabilities[len(dict_sort)]
    return lst_sum_probabilities


def float_to_binary(num):
    """
        Возвращает кодовое слово из 0 и 1 для конкретного символа умножением на 2 дробной части
        суммы вероятностей предыдущих символов
        и делением на 2 целой части суммы вероятностей предыдущих символов
    """
    exponent = 0  # показатель степени
    shifted_num = num  # сумма вероятностей предыдущих символов для текущего символа
    while shifted_num != int(shifted_num):  # пока число не целое
        shifted_num *= 2
        exponent += 1
    if exponent == 0:  # если число целое
        return '{0:0b}'.format(int(shifted_num))  # возвращает двоичные представления целых чисел
    binary = '{0:0{1}b}'.format(int(shifted_num), exponent + 1)  # В 0{1}строке формата задается ширина
    # отформатированной строки {1}, дополняемая слева нулями
    integer_part = binary[:-exponent]  # все, что идет до exponent - это целая часть
    fractional_part = binary[-exponent:].rstrip('0')  # все, что идет после exponent, включая его - дробная часть(
    # rstrip() убирает в конце нолики)
    return '{0}.{1}'.format(integer_part, fractional_part)


def get_dict_with_codes(lst_sum_probabilities, lst_symbols, lst_length_i):
    """
        Возвращает словарь из символов и соответствующих им кодовых слов, проходя по суммам
        вероятностей предыдущих символов и вызывая для каждой суммы функцию float_to_binary(), которая переводит сумму
        из десятичной системы в двоичную систему счисления,
    """
    lst_code = []
    for i, j in enumerate(lst_sum_probabilities):
        code = f'{float_to_binary(j)[2:]}'  # делаю срез, потому что нужно убрать первый 0 и
        # точку (пример: 0.10011001100110011001100110011001100110011001100110011)
        while len(code) < lst_length_i[i]:
            code += '0'
        if len(code) > lst_length_i[i]:
            code = code[:lst_length_i[i]]  # срезаем код до этой длины
        lst_code.append(code)
    dict_with_code_words = dict()
    for number_1, j in enumerate(lst_symbols):
        for number_2, k in enumerate(lst_code):
            if number_1 == number_2:
                dict_with_code_words[j] = k
    return dict_with_code_words


if __name__ == '__main__':
    dict_sort = read_file()
    # print(dict_sort)
    try:
        lst_symbols = get_symbols(dict_sort)
        # print(lst_symbols)
        lst_length_i = get_lst_with_length(dict_sort)
        # print(lst_length_i)
        lst_sum_probabilities = get_sum_probabilities(dict_sort)
        # print(lst_sum_probabilities)
        dict_with_code_words = get_dict_with_codes(lst_sum_probabilities, lst_symbols, lst_length_i)
        # print(dict_with_code_words)

        while True:
            code_word = input(f"Введите слово, которое хотите закодировать, с пробелами, "
                              f"используя символы из заполненного вами файла: {lst_symbols}:\n").split()
            if not code_word:
                break
            sequense_code = ''
            len_code_word = 0
            for k in code_word:
                try:
                    sequense_code += dict_with_code_words[k] + ' '
                    len_code_word += 1
                except:
                    print(
                        f"Ошибка: Нужно вводить слово, состоящее только из символов алфавита c пробелами: {lst_symbols}")
                    break
            if len_code_word == len(code_word):
                print(sequense_code)
    except:
        print("Проверьте, что файл 'text.txt' существует. Заполните файл 'text.txt' правильно. Количество символов "
              "алфавита и количество вероятностей должны быть "
              "равными.")
