def read_file():
    """
    Читает файл text_2.txt и
    возвращает список из символов алфавита, написанных в данном файле
    """
    lst_symbols = []
    try:
        with open('text_2.txt', encoding='utf8') as f:
            lines = f.readlines()
            if not lines:
                return
            line_1 = lines[0].split()
            for symbol in line_1:
                lst_symbols.append(symbol)
    except:
        return
    return lst_symbols


def get_word_with_number_bu(output_word, number_string):
    """
    Принимает декодированное с помощью стопки книг слово и выходной номер кодируемого слова из Б-У,
    с помощью них возвращает декодированное слово
    """
    lst_symbol = []
    length_symbol = 1
    for i in output_word:
        lst_symbol.append(i)
    sorted_lst_symbol = sorted(lst_symbol)
    new_lst_symbol = sorted_lst_symbol
    while length_symbol != len(output_word):
        lst_symbol_2 = []
        for k in range(len(lst_symbol)):
            new_symbol = lst_symbol[k] + " " + new_lst_symbol[k]
            lst_symbol_2.append(new_symbol)
        new_lst_symbol = lst_symbol_2
        new_lst_symbol = sorted(new_lst_symbol)
        length_symbol += 1
    return new_lst_symbol[number_string - 1]


if __name__ == '__main__':
    """
    Реализация стопки книг тут
    """
    lst_symbols = read_file()
    try:
        while True:
            code_word = input(f"Введите последовательность кодовых слов для декодирования через пробел, используя "
                              f"кодовые слова: от '0' до '{len(lst_symbols) - 1}':\n").split()
            if not code_word:
                break
            # output_string_bu = input(
            #     f"Введите выходное слово при кодировании БУ в виде символов через пробел, используя символы из "
            #     f"алфавита: "
            #     f"{lst_symbols}:\n").split()
            output_number_str = input(
                "Введите выходной номер строки кодируемого слова при кодировании БУ:\n")
            if not output_number_str:
                break
            elements = ''
            count_prev_code_words = 0
            lst_symbols = read_file()
            for i in code_word:
                try:
                    elements += lst_symbols[int(i)] + ''
                    lst_symbols.insert(0, lst_symbols[int(i)])
                    del lst_symbols[int(i) + 1]
                    count_prev_code_words += 1
                except:
                    print(
                        f"Ошибка: введите последовательность кодовых слов от '0' до '{len(lst_symbols) - 1}' через пробел")
                    break
            if count_prev_code_words == len(code_word):
                print(elements)
                print(get_word_with_number_bu(elements,
                                              int(output_number_str)))
    except:
        print(f"Проверьте, что файл 'text_2.txt' существует. Заполните файл 'text_2.txt' правильно. "
              f"Файл не должен быть пустым.\nТакже напоминание: Нужно вводить выходное слово из Б-У,"
              f" состоящее только из символов алфавита {lst_symbols} c пробелами")
